/**
 * Esta clase es la que implementa la insercion en la base de datos
 * @author: Ivan Alberto Novella Lopez, novellaivan1@gmail.com
 * @version: 05/01/2017/ A
 * @see Inicio.java, el cual le manda el objeto tipo Prueba1 para insertarlo en la base de datos
 * @see En persistence.xml contiene los valores de la base de datos y la conexion que se hizo
 */
package implementa;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import model.Prueba1;

public class InsertaDB{
	/**
     * Variables de persistencia, para la conexion y el manejo de las entidades (pojos) modelados con JPA
     */
	static EntityManagerFactory entityManagerFactory;
	static EntityManager entityManajer;
	
	/**
     * Metodo encargado para insertar el objeto en la base de datos
     * @param Se inserta en la base de datos, extrayendo del objeto todos los valores
     */
	public int Inserta(List<Prueba1> listaprueba){
		
		entityManagerFactory = Persistence.createEntityManagerFactory("PruebaXML");
		entityManajer = entityManagerFactory.createEntityManager();
		
		int x=0;
		try {
			if (listaprueba != null) {
				entityManajer.getTransaction().begin();
				for (int i=0; i<listaprueba.size(); i++) {
					Prueba1 prueba =(Prueba1) listaprueba.get(i);
					entityManajer.persist(prueba);
				}
				entityManajer.getTransaction().commit(); 
			}
			x=1;
		} catch (Exception e) {
			System.out.println("insertarUsuario e: " +e);
			x=0;
		}
		return x;
	}
}
