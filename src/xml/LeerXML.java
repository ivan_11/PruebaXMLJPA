/**
 * Esta clase es la que lee los valores del XML
 * @author: Ivan Alberto Novella Lopez, novellaivan1@gmail.com
 * @version: 05/01/2017/ A
 * @see Inicio.java, le manda el objeto listaPrueba en el que se guardaran los datos
 * @see En persistence.xml contiene los valores de la base de datos y la conexion que se hizo
 */
package xml;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import model.Prueba1;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;


public class LeerXML {	
	
	public List<Prueba1> leerXML () {
		List<Prueba1> listaPrueba = new ArrayList<Prueba1>();
		try {
			//Se crea un SAXBuilder para poder parsear el archivo
			SAXBuilder cargarXml = new SAXBuilder();
			File xmlFile = new File( "sanmina.xml" );
			//Se crea el documento a traves del archivo
			Document document = cargarXml.build(xmlFile);
			//Se obtiene la raiz
			Element elemntXml = document.getRootElement();
			//Se obtiene la lista de hijos de la raiz
			List<Element> lista = elemntXml.getChildren(); 
			if (lista != null) {
				//Se recorre la lista de hijos
				for(Element hijo: lista){   
					if (hijo.getName().equals("element")) {
						List<Element> listaComplexType = hijo.getChildren(); 
						if (listaComplexType != null) {
							for (int i = 0; i < listaComplexType.size(); i++) {
								Element elemtComplext = (Element) listaComplexType.get(i);
								if (elemtComplext.getName().equals("complexType")) {
									List<Element> listaAttribute = elemtComplext.getChildren();
									if (listaAttribute != null) {
										//Se obtienen los valores que estan entre los tags
										for (int x= 0; x<listaAttribute.size(); x++){
											Element elemtAttribute = (Element) listaAttribute.get(x);
											Prueba1 prueba = new Prueba1();
											if (elemtAttribute.getAttribute("name") != null) {
												prueba.setDescripcion(elemtAttribute.getAttribute("name").getValue());
											} 
											if (elemtAttribute.getAttribute("type") != null) {
												prueba.setTipo(elemtAttribute.getAttribute("type").getValue());
											}
											if (elemtAttribute.getAttribute("use") != null) {
												prueba.setRequerido(elemtAttribute.getAttribute("use").getValue());
											}
											listaPrueba.add(prueba);
										}
									}
								}
							}
						}
					}
				}
			}
			
		} catch (Exception e) {
		}
		return listaPrueba;
	}
}
