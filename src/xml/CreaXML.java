/**
 * Esta clase es la que crea el archivo XML 
 * @author: Ivan Alberto Novella Lopez, novellaivan1@gmail.com
 * @version: 05/01/2017/ A
 * @see Inicio.java
 */
package xml;

import java.io.File;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;


public class CreaXML {	
	
	public int crearXml () {
		int x=0;
		try {
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document XML = docBuilder.newDocument();
			Element ElmtSchema = XML.createElementNS("http://www.w3.org/2001/XMLSchema","xs:schema");
				ElmtSchema.setAttribute("xmlns:xs",  "http://www.w3.org/2001/XMLSchema");
				ElmtSchema.setAttribute("elementFormDefault", "qualified"); 
				Element ElmtElement = XML.createElementNS("http://www.w3.org/2001/XMLSchema","xs:element");
					ElmtElement.setAttribute("name", "AP");
				
					Element ElmtContexType = XML.createElementNS("http://www.w3.org/2001/XMLSchema", "xs:complexType");
					ElmtElement.appendChild(ElmtContexType);
					Element ElmtAttribute = XML.createElementNS("http://www.w3.org/2001/XMLSchema", "xs:attribute");
						ElmtAttribute.setAttribute("name", "schemaLocation");
						ElmtAttribute.setAttribute("type","xs:string");
					ElmtContexType.appendChild(ElmtAttribute);
					Element ElmtAttributeUno = XML.createElementNS("http://www.w3.org/2001/XMLSchema", "xs:attribute");
						ElmtAttributeUno.setAttribute("name", "PONumber");
						ElmtAttributeUno.setAttribute("type","xs:string");
					ElmtContexType.appendChild(ElmtAttributeUno);
					Element ElmtAttributeDos = XML.createElementNS("http://www.w3.org/2001/XMLSchema", "xs:attribute");
						ElmtAttributeDos.setAttribute("name", "email");
						ElmtAttributeDos.setAttribute("type","xs:string");
						ElmtAttributeDos.setAttribute("use","required");
					ElmtContexType.appendChild(ElmtAttributeDos);
					Element ElmtAttributeTres = XML.createElementNS("http://www.w3.org/2001/XMLSchema", "xs:attribute");
						ElmtAttributeTres.setAttribute("name", "LegalEntityName");
						ElmtAttributeTres.setAttribute("type","xs:string");
						ElmtAttributeTres.setAttribute("use","required");
					ElmtContexType.appendChild(ElmtAttributeTres);
					Element ElmtAttributeCuatro = XML.createElementNS("http://www.w3.org/2001/XMLSchema", "xs:attribute");
						ElmtAttributeCuatro.setAttribute("name", "CustomerCode");
						ElmtAttributeCuatro.setAttribute("type","xs:string");
					ElmtContexType.appendChild(ElmtAttributeCuatro);
					Element ElmtAttributeCinco = XML.createElementNS("http://www.w3.org/2001/XMLSchema", "xs:attribute");
						ElmtAttributeCinco.setAttribute("name", "Currency");
						ElmtAttributeCinco.setAttribute("type","xs:string");
					ElmtContexType.appendChild(ElmtAttributeCinco);
					Element ElmtAttributeSeis = XML.createElementNS("http://www.w3.org/2001/XMLSchema", "xs:attribute");
						ElmtAttributeSeis.setAttribute("name", "ExchangeRate");
						ElmtAttributeSeis.setAttribute("type","xs:string");
						ElmtAttributeSeis.setAttribute("use","required");
					ElmtContexType.appendChild(ElmtAttributeSeis);
					Element ElmtAttributeSiete = XML.createElementNS("http://www.w3.org/2001/XMLSchema", "xs:attribute");
						ElmtAttributeSiete.setAttribute("name", "InternalInvoiceNumber");
						ElmtAttributeSiete.setAttribute("type","xs:string");
						ElmtAttributeSiete.setAttribute("use","required");
					ElmtContexType.appendChild(ElmtAttributeSiete);
				ElmtSchema.appendChild(ElmtElement);
			XML.appendChild(ElmtSchema);
			
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer;
			transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(XML);	
			StreamResult result = new StreamResult(new File("sanmina.xml"));
			transformer.transform(source, result); 
			x=1;
		} catch (Exception e) {
			x=0;
			System.out.println("crearXml e: " +e);
		}
		return x;			
	}
}
