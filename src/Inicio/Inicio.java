/**
 * Esta clase es el main, se usa para crear un archivo XML, despues Leerlo y por ultimo Guardarlo en una base de datos
 * @author: Ivan Alberto Novella Lopez, novellaivan1@gmail.com
 * @version: 05/01/2017/ A
 * @see InsertaDB, LeerXML, CreaXML y Prueba1
 * @see En persistence.xml contiene los valores de la base de datos y la conexion que se hizo
 */
package Inicio;
import implementa.InsertaDB;
import java.util.ArrayList;
import java.util.List;
import xml.CreaXML;
import xml.LeerXML;
import model.Prueba1;

/**
 * Clase General, mantiene comunicacion con todas las demas clases
 */
public class Inicio {		
	
	 /**
     * M�todo que ejecuta la creacion, lectura y insercion en la BD
     */
	public static void main (String[] args) {
		/**
	     * Constructor para crear el archivo XML
	     * @param se toma en cuenta la variable x, para saber si se creo el archivo o no
	     * @see src/xml/CreaXML.java
	     */
		CreaXML crea = new CreaXML();
		int x = crea.crearXml();		
		if (x == 1) {
			/**
		     * Constructor del objeto Prueba1, que es el pojo que se genero de la base de datos
		     * @param se crea la lista para que usando el obejto se puedan guardar los valores
		     * @see src/model/Prueba1.java
		     */
			List<Prueba1> listaprueba = new ArrayList<Prueba1>();
			/**
		     * Constructor para poder hacer la lectura del XML
		     * @param Se crea para hacer la lectura con la clase LeerXML y con el metodo leerXML()
		     * se usa listaprueba para guardar en este objeto lo que leimos del XML
		     * @see src/xml/LeerXML.java
		     */
			LeerXML leer = new LeerXML();
			listaprueba = leer.leerXML();
			/**
		     * Constructor para poder insertar en la base de datos, los valores leidos del XML
		     * @param Se para poder hacer la insercion en la base de datos, a su vez se le manda el objeto 
		     * listaprueba para que se inserte los valores que se obtuvieron 
		     * despues de la lectura del XML
		     * @see src/implementa/InsertaDB.java
		     */
			InsertaDB inserta = new InsertaDB();
			int y = inserta.Inserta(listaprueba);
			if (y==1) {
				System.out.println("Se genero e inserto el XML en la base de datos");
			}
		}
	}
}
