/**
 * Este es el pojo u objeto generado desde la base de datos con JPA
 * @author: Ivan Alberto Novella Lopez, novellaivan1@gmail.com
 * @version: 05/01/2017/ A
 * @see
 */
package model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the prueba1 database table.
 * 
 */
@Entity
@NamedQuery(name="Prueba1.findAll", query="SELECT p FROM Prueba1 p")
public class Prueba1 implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	private String descripcion;

	private String requerido;

	private String tipo;

	public Prueba1() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getRequerido() {
		return this.requerido;
	}

	public void setRequerido(String requerido) {
		this.requerido = requerido;
	}

	public String getTipo() {
		return this.tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

}